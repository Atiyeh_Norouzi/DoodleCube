﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chunkPooler : MonoBehaviour {
    GameObject chunksParent;
    public enum chunkType { easyChunk=0 , normalChunk , hardChunk , masterChunk}
    [System.Serializable]
    public struct chunkStruct
    {
         public chunkType type;
         public GameObject [] prefab;
         public int poolamount;
         public List<GameObject> pooledObjects;
    }
    #region sources
    
    public chunkStruct[] ChunksWeWantToPool;
    private static chunkPooler _instance;
    public static chunkPooler instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<chunkPooler>();
            return _instance;
        }
    }

    #endregion
    void Awake()
    {
        _instance = this;
        chunksParent = new GameObject("ChunkPooler");
    }
    void Start()
    {
        CreatePools();
    }
    public void CreatePools()
    {
        for (int i = 0; i < ChunksWeWantToPool.Length; i++)
        {

            ChunksWeWantToPool[i].pooledObjects = new List<GameObject>();
            GameObject objParent = new GameObject(ChunksWeWantToPool[i].type + "_parent");
            objParent.transform.SetParent(chunksParent.transform);
            for (int j = 0; j < ChunksWeWantToPool[i].poolamount; j++)
            {
                if(j < ChunksWeWantToPool[i].prefab.Length)
                    Spawn(i, j ,objParent);
                else
                    Spawn(i, getRandom(0 , ChunksWeWantToPool[i].prefab.Length), objParent);
            }
        }
    }
    public void Spawn(int i,  int index ,  GameObject parent)
    {
   
        GameObject GO = Instantiate(ChunksWeWantToPool[i].prefab[index]);
        GO.SetActive(false);
        GO.transform.SetParent(parent.transform);
        ChunksWeWantToPool[i].pooledObjects.Add(GO);
    }
    //get a random Scene from objectsPooles
    public GameObject GetPooledObject(chunkPooler.chunkType type)
    {

        int chunkType = (int)type;
        int rand = getRandom(0 , ChunksWeWantToPool[chunkType].prefab.Length);
        int count = 0;
        GameObject obj = ChunksWeWantToPool[chunkType].pooledObjects[rand];


        while (obj.activeSelf)
        {
            rand = getRandom(0, ChunksWeWantToPool[chunkType].prefab.Length);
            obj = ChunksWeWantToPool[chunkType].pooledObjects[rand];

        }
        obj.transform.SetParent(null);
        obj.SetActive(true);
        return obj;
    }
    public void deactive(GameObject obj, int num)
    {
        obj.SetActive(false);
        obj.transform.SetParent(chunksParent.transform.GetChild(num).transform);
        obj.transform.localPosition = Vector3.zero;
    }
    public int getRandom(int start , int end)
    {
        return Random.Range(start, end);
    }
    
}

