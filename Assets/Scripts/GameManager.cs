﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    [SerializeField]
    private Transform _characterPosition;
    [SerializeField]
    private Transform _DespawnZone;
    [SerializeField]
    private Transform _rightZone;
    [SerializeField]
    private Transform _leftZone;
    [SerializeField]
    private Camera _camera;
    //Instance
    private static GameManager _instance;
    public static GameManager instance
    {
        get
        {
            if (_instance == null)
                _instance = FindObjectOfType<GameManager>();

            return _instance;
        }
    }
    public Transform CharacterPos
    {
        get
        {
            return _characterPosition;
        }
    }
    public Transform LeftZonePos
    {
        get
        {
            return _leftZone;
        }
    }
    public Transform RightZonePos
    {
        get
        {
            return _rightZone;
        }
    }
    
    public Transform DeadZonePos
    {
        get
        {
            return _DespawnZone;
        }
    }
    public Camera camera
    {
        get
        {
            return _camera;
        }
    }

}
