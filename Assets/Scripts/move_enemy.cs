﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move_enemy : MonoBehaviour ,  IMovable {

    public Vector3 rightPosition;
    public Vector3 leftPosition;
    bool moveright;
    private Vector3 target;
    //distance from the tiles
    private const float distance = 1f;
    private const string rightneighbourSTR = "right_neighbour";
    private const string leftneighbourSTR = "left_neighbour";
    // Use this for initialization
    private Vector3 dir;

    //must be commeted
    void Start()
    {
        init();
    }
    public void init()
    {
        setNeighbours();
        target = leftPosition;
        dir = Vector3.left;
        moveright = false;
        StartCoroutine(Move((target - transform.position).normalized, 1.2f));
        StartCoroutine(CheckDeadZone());
    }
  

    void setNeighbours()
    {

        leftPosition = gameObject.transform.parent.Find(leftneighbourSTR).transform.position + distance * Vector3.right;
        rightPosition = gameObject.transform.parent.Find(rightneighbourSTR).transform.position + distance * Vector3.left;

    }

    public IEnumerator Move(Vector3 direction, float speed)
    {
        while (true)
        {
           
            target.y = transform.position.y;
            if (Utility.almostEqual(transform.position.x, target.x, 0.1f))
            {
                target.x = moveright ? leftPosition.x : rightPosition.x;
                dir = (target - transform.position).normalized;
                moveright = !moveright;

            }
            else
            {
                transform.Translate(dir * speed * TimeManager.deltaTime);
            }
            yield return null;
        }
    }


    public void Despawn()
    {
        ObjectPooler.instance.pushMove(this);
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "cube")
        {
            CameraController.OnTarget = true;
            col.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            col.gameObject.GetComponent<Character>().enabled = false;
            GameState.instance.Pause();
        }
        if (col.gameObject.tag == "bullet")
            Despawn();

    }

    public IEnumerator CheckDeadZone()
    {
        while (true)
        {
            if(transform.position.y <= GameManager.instance.DeadZonePos.position.y)
                Despawn();
            yield return new WaitForEndOfFrame();
        }
       
    }
}
