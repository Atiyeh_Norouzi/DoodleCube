﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimation : MonoBehaviour {
    private Animator _animator;
    private static CharacterAnimation _instance;
    public class animationStates
    {
        public static string move_right = "move_right";
        public static string move_left = "move_left";
        public static string jump = "jump";
        public static string fall = "fall";

    }

    private string currentState = animationStates.jump;
    void Start()
    {
        _instance = this;
        _animator = GetComponent<Animator>();
     
    }

    public static CharacterAnimation instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<CharacterAnimation>();
            return _instance;
        }

    }

    private Animator animator
    {
        get
        {
            if (_animator == null)
                _animator = GetComponent<Animator>();
            return _animator;
        }
    }

    public void startAnimation(string state)
    {
       
            currentState = state;
            animator.SetBool("play", true);
            animator.SetBool("move_right", state == animationStates.move_right);
            animator.SetBool("move_left", state == animationStates.move_left);
            animator.SetBool("fall", state == animationStates.fall);
            animator.SetBool("jump", state == animationStates.jump);
            StartCoroutine(OnExitAnimation(currentState, animator.GetCurrentAnimatorStateInfo(0).length));
       

    }
    IEnumerator OnExitAnimation(string state, float delay)
    {
       
        yield return new WaitForSeconds(delay);
        animator.SetBool(state , false);


    }
}
