﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {

    private const float speed = 20f;
    private const string upneighbourSTR = "up_neighbour";
    private const string downneighbourSTR = "down_neighbour";
    private const string rightneighbourSTR = "right_neighbour";
    private const string leftneighbourSTR = "left_neighbour";

    public static string static_pr = "static_parents";
    public static string bounce_pr = "bounce_parents";
    public static string movevr_pr = "movevr_parents";
    public static string movehr_pr = "movehr_parents";

    private float leftNeighbour;
    private float rightNeighbour;
    private float _upNeighbour;
    private float _downNeighbour;
    

    private float offset = 0.05f;
    private Vector3 dir;
    private Rigidbody2D _rb;
    private bool CanMove = false;
    private bool moveright;
    private bool moveup;
    private bool HorizontalMove = false;
    private Vector3 target;
    private TileAnimationController _animationController;


    public enum tileState { STATIC = 0 , BREAK ,  MOVE , BOUNCE , ROTATE , MOVEUP}
    public  tileState currentState;
    private Rigidbody2D rb
    {
        get
        {   if (_rb == null)
                _rb = GetComponent<Rigidbody2D>();
            return _rb;
        }
    }
    private TileAnimationController animationController
    {
        get
        {
            if (_animationController == null)
                _animationController = GetComponent<TileAnimationController>();
            return _animationController;
        }
    }

    //must be commented
    void Start()
    {
        _animationController = GetComponent<TileAnimationController>();
        init(currentState);
    }
	public void init(tileState state)
    {
        currentState = state;
        CanMove = false;
        switch (state)
        {

            case tileState.MOVE:
                {
                  
                    setNeighbours(true);
                    target = new Vector3(leftNeighbour , transform.position.y , transform.position.z);
                    InitializeMove(Vector3.left, leftNeighbour, true);
                    break;
                }
            case tileState.MOVEUP:
                {

                    setNeighbours(false);
                    target = new Vector3(_downNeighbour, transform.position.y, transform.position.z);
                    InitializeMove(Vector3.down, _downNeighbour, false);
                    break;
                }
        }
        StartCoroutine(CheckDeadZone());
    }
    void InitializeMove(Vector3 direction , float target , bool hMove)
    {
        CanMove = true;
        HorizontalMove = hMove;
        dir = direction;
      
    }
    void setNeighbours(bool horizontalmove)
    {
        if (horizontalmove)
        {
            leftNeighbour = gameObject.transform.parent.Find(leftneighbourSTR).transform.position.x - offset;
            rightNeighbour = gameObject.transform.parent.Find(rightneighbourSTR).transform.position.x + offset;
        }
        else
        {
            _upNeighbour = gameObject.transform.parent.Find(upneighbourSTR).transform.position.y - offset;
            _downNeighbour = gameObject.transform.parent.Find(downneighbourSTR).transform.position.y + offset;
        }

    }
    void OnCollisionEnter2D(Collision2D col)
    {

        if (col.gameObject.tag == "cube")
            animationController.startAnimation(currentState == tileState.BREAK ? TileAnimationController.animationState.Break :
                (currentState == tileState.ROTATE ? TileAnimationController.animationState.Rotate : TileAnimationController.animationState.None));

    }
    
    void FixedUpdate()
    {
    
        #region change tile direction move
        if (HorizontalMove)
        {
            target.y = transform.position.y;
            if (Utility.almostEqual(transform.position.x, target.x, 0.1f))
            {
                target.x = moveright ? leftNeighbour : rightNeighbour;
                dir = (target - transform.position).normalized;
                moveright = !moveright;
            }
        }
        else
        {
            target.x = transform.position.x;
            if (Utility.almostEqual(transform.position.y, target.y, 0.1f))
            {
                target.y = moveup ? _downNeighbour : _upNeighbour;
                dir = (target - transform.position).normalized;
                moveup = !moveup;
            }
        }
        #endregion

        #region tileMove
        if (CanMove)
        {
            transform.Translate(dir * TimeManager.deltaTime);
            //rb.MovePosition( dir*TimeManager.deltaTime );
        }
        #endregion
        
    }

    public void Despawn()
    {
        CanMove = false;
        currentState = tileState.STATIC;
        ObjectPooler.instance.pushTile(this);
    }
 
    public IEnumerator CheckDeadZone()
    {
        while (true)
        {
            if(transform.position.y <= GameManager.instance.DeadZonePos.position.y)
                Despawn();
            yield return new WaitForEndOfFrame();
        }
    }
}
