﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class MenuController : MonoBehaviour {
    public Button startBtn;
    public Button quitBtn;
    
    
	// Use this for initialization
	void Start ()
    {
       
        startBtn.onClick.AddListener(delegate(){ StartGame(); });
        quitBtn.onClick.AddListener(delegate(){ quitGame(); });
	}
	
	public void StartGame()
    {
    
        SceneManager.LoadScene("DoodleCube");
    }
    public void quitGame()
    {
      
        Application.Quit();
    }
  
}
