﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fire_enemy : MonoBehaviour , IStatic  {
    bool isEnabled = true;
    private SpriteRenderer renderer;
	// Use this for initialization

    void Start()
    {
        renderer = gameObject.GetComponent<SpriteRenderer>();
        init();
    }
    public void init()
    {
        isEnabled = true;
        InvokeRepeating("Shoot", 0, 2f);
        StartCoroutine(CheckDeadZone());
    }
    void Shoot()
    {
        if (renderer.isVisible)
        {
            Bullet obj = ObjectPooler.instance.GetEBullet();
            obj.transform.position = transform.position;
            Vector3 dir = (GameManager.instance.CharacterPos.position - transform.position).normalized;
            obj.init(dir, 4f);
        }
    }


    public void Despawn()
    {
        ObjectPooler.instance.pushFire(this);

        CancelInvoke("Shoot");
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "cube")
            GameState.instance.Pause();
        else if (col.gameObject.tag == "bullet")
            Despawn();
    }

    public IEnumerator CheckDeadZone()
    {
        while (isEnabled)
        {
            if(transform.position.y <= GameManager.instance.DeadZonePos.position.y)

            {
                isEnabled = false;
                Despawn();
            }
            yield return new WaitForEndOfFrame();
        }
    }
}
