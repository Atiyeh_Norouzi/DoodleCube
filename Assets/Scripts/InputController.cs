﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputController : MonoBehaviour 
{
    public int testnum = 0;

    private const float keepKeyValue = .01f;
    private float axisValue;
    //joystick buttons
    public Button right;
    public Button left;
    public Button up;
    bool shoot;

    public enum InputType { RightMove = 0, LeftMove, Jump, Shoot, AnyKey }
    public delegate void Deleg_Input(InputType type);
    public event Deleg_Input _OnPressed;
    public delegate void Deleg_GetAxis(InputType type, float value);
    public event Deleg_GetAxis _GetAxis;
    private static InputController _instance;

    public static InputController instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<InputController>();
            return _instance;
        }

    }

    public void AddTest()
    {
        testnum++;
    }

    void Start()
    {

       
    }
    public void PointerRightDown()
    {   
        axisValue = -1;



        if (_OnPressed != null)
        {


            _GetAxis(InputType.RightMove, axisValue);
        }
    }
    public void OnClicked()
    {


        if (_OnPressed != null)
        {
            

            _OnPressed(InputType.Jump);
            shoot = true;
        }
    }
    public void PointerRightUp()
    {
        axisValue = 0;
        if (_OnPressed != null)
            _GetAxis(InputType.RightMove, axisValue);
    }
    public void PointerLeftDown()
    {
        axisValue = 1;
        if (_OnPressed != null)
            _GetAxis(InputType.LeftMove, axisValue);
    }
    public void PointerLeftUp()
    {
        axisValue = 0;
        if (_OnPressed != null)
            _GetAxis(InputType.LeftMove, axisValue);
    }
    public void OnButtonTouching(PointerEventData data)
    {
      
    
        if (data.button.Equals(right))
        {
            if (axisValue > keepKeyValue)
                if (_OnPressed != null)
                    _GetAxis(InputType.RightMove, axisValue);
        }
        else if(data.button.Equals(left))
        {
            if (axisValue < -keepKeyValue)
                if (_OnPressed != null)
                    _GetAxis(InputType.LeftMove, axisValue);
        }
    }
    void Update()
    {
      
        axisValue = Input.GetAxis("Horizontal");
        if (Input.anyKey || Input.touchCount > 0 )
            if(_OnPressed != null)
                _OnPressed(InputType.AnyKey);
        if (axisValue > keepKeyValue)
            if (_OnPressed != null)
                _GetAxis(InputType.RightMove, axisValue);
        if (axisValue < -keepKeyValue)
            if (_OnPressed != null)
                _GetAxis(InputType.LeftMove , axisValue);
        if (Input.GetKeyDown(KeyCode.Space))
            if (_OnPressed != null)
                _OnPressed(InputType.Jump);
        if(!shoot)
        {
            if (Input.GetMouseButtonDown(0))
                if (_OnPressed != null)
                    _OnPressed(InputType.Shoot);
        }  
      
    }

  
}
