﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class direction : MonoBehaviour {
    
    Vector3 dir;
    Vector3 mousePositionInWorldPoint;
    Vector3 Character_pos;
    // Use this for initialization
    void Start () {
        #region setDir
        mousePositionInWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
         Character_pos = GameManager.instance.CharacterPos.position;
         dir = (mousePositionInWorldPoint - Character_pos).normalized;
        #endregion
    }
	
	// Update is called once per frame
	void Update () {
        #region rotateDirectionTowardMouseDir
        mousePositionInWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        dir = (mousePositionInWorldPoint - Character_pos).normalized;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        #endregion
    }

 
}
