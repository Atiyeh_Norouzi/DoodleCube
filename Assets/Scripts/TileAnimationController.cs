﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileAnimationController : MonoBehaviour
{
    private Animator _animator;
    public enum animationState { Idle =0 , Rotate , Break ,None }
    private animationState currentState;

    void Start()
    {
     
        _animator = GetComponent<Animator>();
    }
    private Animator animator
    {
        get
        {
            if (_animator == null)
                _animator = GetComponent<Animator>();
            return _animator;
        }
    }
    public  void startAnimation(animationState state)
    {
        if (state != animationState.None)
          {

            animator.SetBool("move", true);
            animator.SetBool("Rotate", state == animationState.Rotate);
            animator.SetBool("Break", state == animationState.Break);
         }
            
    }
  
}
