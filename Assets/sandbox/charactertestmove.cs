﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class charactertestmove : MonoBehaviour {
    bool firstTimeVelocitySave = false;
    Vector3 savedVelocity;
    float BounceRate = 2f;
    // Use this for initialization
    void Start () {
	   
	}
	
	// Update is called once per frame
	void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
 
            GetComponent<Rigidbody2D>().AddForce(Vector2.up *300f);
        }
        if (GetComponent<Rigidbody2D>().velocity.y > 0.1f)
            gameObject.layer = LayerMask.NameToLayer("brick");
        else
            gameObject.layer = LayerMask.NameToLayer("cube");
        float x = Input.GetAxis("Horizontal") * TimeManager.deltaTime * 100;
        GetComponent<Rigidbody2D>().velocity = new Vector2(x, GetComponent<Rigidbody2D>().velocity.y);
    }
    //void OnCollisionEnter2D(Collision2D col)
    //{
    //    //print("Collision! " + rigidbody.velocity);
    //    if (!firstTimeVelocitySave)
    //    {

    //        savedVelocity = new Vector3(GetComponent<Rigidbody2D>().velocity.x , 10 , 0);
    //        firstTimeVelocitySave = true;
    //        Debug.Log("saved vel" + savedVelocity);
    //    }
    //    GetComponent<Rigidbody2D>().velocity =savedVelocity;

    //}
    void OnCollisionEnter2D(Collision2D other)
    {


        GetComponent<Rigidbody2D>().velocity = Vector3.Reflect(new Vector3(other.relativeVelocity.x, -5, 0), Vector3.up);

    }

}
